import { MailerService } from "@nestjs-modules/mailer";
import { OnQueueActive, OnQueueCompleted, OnQueueProgress, Process, Processor } from "@nestjs/bull";
import { Job } from "bull";
import { CreateUserDTO } from "src/create-user/create-user-dto";
import ejs from 'ejs';
import path from "path";
import { NotFoundException } from "@nestjs/common";

@Processor('sendMail-queue')
class SendMailConsumer {
  constructor(private mailService: MailerService){};

	@Process('sendMail-job')
	async sendMailJob(job: Job<CreateUserDTO>){
		const { data } = job;
		//text: `Olá ${data.name}, seu cadastro foi realizado com sucesso. Seja bem vindo(a)!`
		const template = `${path.resolve('src/public/templates/register.mail.ejs')}`;

		ejs.renderFile(template, data, async (error, htmlrender) => {
			if(error){
				throw new NotFoundException(`error: ${error}`);
			}
			await this.mailService.sendMail({
				to: data.email,
				from: 'Equipe code/Drops <codedrops@codedrops.com.br>',
				subject: 'Seja bem vindo(a)!',
				html: htmlrender,
			});
		});

	}

	@OnQueueCompleted()
	onCompleted(job: Job) {
		console.log(`On Completed ${job.name}`);
	}

	@OnQueueProgress()
	OnQueueProgress(job: Job){
		console.log(`On Progress ${job.name}`);
	}

	@OnQueueActive()
	OnQueueActive(job: Job){
		console.log(`On active ${job.name}`);
	}

}

export { SendMailConsumer }